#!/usr/bin/perl
# Special purpose plugin that takes advantage of ikiwiki being configured
# as the 404 handler, and *always* displays missingsite.tmpl.
#
# One special-purpose site per web server can be configured to use this
# plugin, and set as apache's default site. This way, attempts to access
# sites that have been deleted or are not created yet will display a
# useful error message.
#
# Put this in the apache config template for the missingsite (eg, in
# /etc/ikiwiki-hosting/config/b-missingsite/apache.conf.tmpl):
# DirectoryIndex index.html ikiwiki.cgi
package IkiWiki::Plugin::missingsite;

use warnings;
use strict;
use IkiWiki 3.00;
use IkiWiki::Hosting;

sub import {
	hook(type => "cgi", id => 'missingsite',  call => \&cgi);
	hook(type => "getsetup", id => 'missingsite',  call => \&getsetup);
	hook(type => "genwrapper", id => 'missingsite',  call => \&genwrapper);
}

sub getsetup () {
	return
		plugin => {
			safe => 0,
			rebuild => 0,
		}
}

sub cgi ($) {
	my $cgi=shift;

	print STDERR "missing site: $ENV{SERVER_NAME}\n";

	my $template=IkiWiki::Hosting::ikisite_template("missingsite.tmpl");
	$template->param(SERVER_NAME => $ENV{SERVER_NAME});
	IkiWiki::cgi_custom_failure(
		$cgi,
		"404 Not Found",
		$template->output,
	);
}

sub genwrapper () {
	# Pass SERVER_NAME through the wrapper.
	return <<EOF;
		if ((s=getenv("SERVER_NAME")))
			addenv("SERVER_NAME", s);
EOF
}

1;
