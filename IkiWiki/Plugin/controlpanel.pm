#!/usr/bin/perl
# Allows do=controlpanel to be used to view a control panel of all sites.
package IkiWiki::Plugin::controlpanel;

use warnings;
use strict;
use IkiWiki 3.00;

sub import {
	hook(type => "sessioncgi", id => "controlpanel", call => \&sessioncgi);
}

sub sessioncgi ($$) {
	my $cgi=shift;
	my $session=shift;

	return unless defined $cgi->param("do") &&
		$cgi->param("do") eq "controlpanel";
	
	IkiWiki::needsignin($cgi, $session);

	# Drop ikiwiki lock to avoid blocking other users.
	IkiWiki::unlockwiki();

	eval q{use IkiWiki::Hosting};
	error $@ if $@;
	eval q{use URI};
	error $@ if $@;

	# A single user can have multiple openids and emails.
	my $username=$session->param("name");
	eval q{use IkiWiki::Customer};
	error $@ if $@;
	my @accounts=IkiWiki::Customer::account_list($username);

	my (@owned_sites, @adminned_sites);
	my $num=0;
	foreach my $siteinfo (@{IkiWiki::Hosting::yamlgetshell(
	                          "ikisite-wrapper", "list", "--extended",
				  (map { "--admin=$_"} @accounts),
				  (map { "--owner=$_"} @accounts),
		              )}) {
		$siteinfo->{cgiurl}=$config{cgiurl};
		$siteinfo->{num}=$num++;

		# dress up raw timestamp
		$siteinfo->{site_created}=displaytime($siteinfo->{site_created});

		my $url=URI->new($siteinfo->{site_url});
		$siteinfo->{site_domain}=$url->host();

		if ($siteinfo->{isowner}) {
			push @owned_sites, $siteinfo;
		}
		else {
			push @adminned_sites, $siteinfo;
		}
	}
	if (@owned_sites) {
		@owned_sites=sortsites(@owned_sites);
		$owned_sites[0]->{first_owned}=1;
		$owned_sites[$#owned_sites]->{last_owned}=1;
	}
	if (@adminned_sites) {
		@adminned_sites=sortsites(@adminned_sites);
		$adminned_sites[0]->{first_adminned}=1;
		$adminned_sites[$#adminned_sites]->{last_adminned}=1;
	}

	my $template=IkiWiki::Hosting::ikisite_template("controlpanel.tmpl");
        $template->param(sites => [@owned_sites, @adminned_sites]);

	if (@owned_sites) {
		# Add price plan and balance info.
		eval q{use IkiWiki::Business};
		error $@ if $@;
		my $plan=IkiWiki::Business::currentplan($username);
		if ($plan) {
			$template->param(site_owned_count => int @owned_sites);
			$template->param("plan_$plan" => 1);
			$template->param(plan_name => IkiWiki::Business::planinfo($plan)->{name});
			$template->param(plan_description => IkiWiki::Business::planinfo($plan)->{description});
			$template->param(plan_maxsites => IkiWiki::Business::planinfo($plan)->{maxsites});
			my $paypal_id=IkiWiki::Business::planinfo($plan)->{paypal_id};
			$template->param(paypal_id => $paypal_id) if defined $paypal_id;
		}
		$template=IkiWiki::Business::balanceinfo($username, $template);
	}

	# Embed the makesite page.
	my $page="makesite";
	my $file = $pagesources{$page};
	if (defined $file) {
		my $type = pagetype($file);
		my $content=IkiWiki::htmlize($page, "", $type,
			IkiWiki::linkify($page, "",
			IkiWiki::preprocess($page, "",
			IkiWiki::filter($page, $page,
			readfile(srcfile($file))))));
		$template->param(makesite => $content);
	}

        IkiWiki::printheader($session);
        print IkiWiki::cgitemplate($cgi, "Control Panel", $template->output);
	exit 0;
}

sub sortsites {
	sort {
		lc $a->{site_wikiname} cmp lc $b->{site_wikiname}
	} @_;
}

1
