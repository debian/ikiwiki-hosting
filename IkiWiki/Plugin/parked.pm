#!/usr/bin/perl
# Special purpose plugin that causes a site to be parked. A parked site
# cannot be branched, and has no accessible content. Remote VCS access is
# disabled too.
package IkiWiki::Plugin::parked;

use warnings;
use strict;
use IkiWiki 3.00;
use IkiWiki::Hosting;

sub import {
	hook(type => "getsetup", id => 'parked', call => \&getsetup);
	hook(type => "refresh", id => 'parked', call => \&refresh);
	hook(type => "genwrapper", id => "parked", call => \&genwrapper,
		first => 1);
	hook(type => "disable", id => 'parked', call => \&disable);
	hook(type => "cgi", id => 'parked', call => \&cgi);

	# Cache both versions of the parked message.
	parked_message();
	parked_message(1);
}

sub getsetup () {
	return
		plugin => {
			safe => 0,
			rebuild => 1,
		},
		parked_message => {
			type => "string",
			description => "An optional message explaining why this site is parked.",
			safe => 0,
			rebuild => 1,
		},
}

my %cached_message;
sub parked_message ($) {
	my $html=shift() ? 1 : 0;

	if (! defined $cached_message{$html}) {
		my $template=IkiWiki::Hosting::ikisite_template("parkedsite.tmpl");
		$template->param(html => $html);
		$template->param(wikiname => $config{wikiname});
	        $template->param(parked_message => $config{parked_message})
			if defined $config{parked_message} && length $config{parked_message};
		$cached_message{$html}=$template->output;
	}
	return $cached_message{$html};
}

my $real_writefile;
sub refresh {
	# Intercept file writes when building the site.
	if (! defined $real_writefile) {
		$real_writefile=\&IkiWiki::writefile;
		inject(name => "IkiWiki::writefile", call => \&writefile_parked);
	}

	# Avoid hardlinking raw files, it bypasses our writefile.
	$config{hardlink}=0;
}

sub genwrapper {
	# Write flag file to disable remote git push and pull.
	writefile("$ENV{HOME}/source.git/parked", "", parked_message(0));

	# Set here so the branchable plugin will disable branchability
	# for parked sites, even when the setup has branchable enabled.
	# (For it to take effect, this hook is run first.)
	$config{branchable}=0;

	# C code to add to wrapper.
	return ""
}

sub disable {
	# Enable remote git push and pull.
	unlink("$ENV{HOME}/source.git/parked");
}

# Replacing these files with a parked message could cause bad things to
# happen, so don't.
my @keyfiles=qw{robots.txt favicon.ico};

# This is a wrapper injected around ikiwiki's real writefile.
# It causes the parked message to be output to files rendered
# to the destdir.
sub writefile_parked ($$$;$$) {
	my $file=shift;
	my $destdir=shift;
	my $content=shift;
	my $binary=shift;
	my $writer=shift;

	if ($destdir eq $config{destdir} && exists $destsources{$file} &&
	    ! grep { $_ eq $file } @keyfiles) {
		my $html=($file=~/\.html$/);
		$real_writefile->($file, $destdir, parked_message($html));
	}
	else {
		$real_writefile->($file, $destdir, $content, $binary, $writer);
	}
}

sub cgi ($) {
	my $cgi=shift;

	print "Content-type: text/html\n\n";
	print parked_message(1);
	exit;
}

1;
