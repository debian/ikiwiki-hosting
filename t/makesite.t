#!/usr/bin/perl
use strict;
use warnings;
no warnings 'redefine';
use IkiWiki;
use IkiWiki::Plugin::makesite;
use Cwd qw(getcwd);
use Test::More 'no_plan';

$config{ikisite_conffile} = getcwd.'/ikiwiki-hosting.conf';

# Synthetic test functions.
sub IkiWiki::Hosting::site_addresses {
	return "2.2.2.2", "fe80::4878:f:21a:2";
}
sub IkiWiki::Hosting::host_address_or_cname {
	my $host=shift;
	my $wanted_cname=shift;

	if ($host eq 'one.com' || $host eq 'www.one.com') {
		return "1.1.1.1";
	}
	elsif ($host eq 'two.com' || $host eq 'www.two.com' || $host eq 'two.two.com') {
		return "2.2.2.2";
	}
	elsif ($host eq 'four.two.com') {
		return "4.4.2.2";
	}
	else {
		return undef;
	}
}
sub IkiWiki::Plugin::makesite::unique_internal_hostname {
	my $base=shift;
	my $domain=shift;

	return "$base.$domain";
}

is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("foo", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_READY, "foo.example.com", undef, "foo"],
	"user requests hostname only");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames(" Foo Bar!  ", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_READY, "foobar.example.com", undef, "Foo Bar!"],
	"user entered wikiname, munged to internal hostname");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("one.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_WRONG, "one-com.example.com", "one.com", "one", "www.one.com"],
	"user entered existing external hostname, dns needs fixing");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("www.one.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_WRONG, "one-com.example.com", "www.one.com", "one", "one.com"],
	"user entered existing external hostname with www prefix, dns needs fixing");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("two.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_READY, "two-com.example.com", "two.com", "two", "www.two.com"],
	"user entered existing external hostname, dns is correct");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("www.two.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_READY, "two-com.example.com", "www.two.com", "two", "two.com"],
	"user entered existing external hostname with www prefix, dns is correct");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("three.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_NEEDED, "three-com.example.com",  "three.com", "three", "www.three.com"],
	"user entered domain that does not yet exist");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("www.three.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_NEEDED, "three-com.example.com", "www.three.com", "three", "three.com"],
	"user entered domain that does not yet exist with www prefix");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("three.co.uk", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_NEEDED, "three-co-uk.example.com", "three.co.uk", "three", "www.three.co.uk"],
	"user entered domain that does not yet exist, under multilevel TLD");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("www.three.co.uk", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_NEEDED, "three-co-uk.example.com", "www.three.co.uk", "three", "three.co.uk"],
	"user entered domain that does not yet exist, under multilevel TLD, with www prefix");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("me.two.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_NEEDED, "me-two-com.example.com", "me.two.com", "me", "www.me.two.com"],
	"user entered nonexistant subdomain of existing domain");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("me.four.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_NEEDED, "me-four-com.example.com", "me.four.com", "me", "www.me.four.com"],
	"user entered nonexistant subdomain of nonexistant domain");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("four.two.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_WRONG, "four-two-com.example.com", "four.two.com", "four", "www.four.two.com"],
	"user entered existant subdomain of existing domain, but dns address needs fixed");
is_deeply([IkiWiki::Plugin::makesite::gen_hostnames("two.two.com", "example.com")],
	[$IkiWiki::Plugin::makesite::DNS_READY, "two-two-com.example.com", "two.two.com", "two", "www.two.two.com"],
	"user entered existant subdomain of existing domain, and dns is ok");
