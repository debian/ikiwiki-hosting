ikiwiki-hosting 0.20220715 released with [[!toggle text="these changes"]]
[[!toggleable text="""  * Avoid directly running init scripts, instead use the service command.
  * Avoid running apache2ctl graceful which may start an apache process
    outside of systemd control (see bug #927302).
    (But note that certbot still uses apache2ctl graceful.)
  * As ssh RSA keys are being deprecated in an upcoming ssh release,
    the gitpush plugin will generate a Ed25519 key for the site if one does
    not yet exist.
  * The gitpush plugin, when listing the site's ssh keys, will list all
    available public keys, rather than just one.
  * Fix anonymous git push to branchable sites, which has been broken
    since 2018 by a git behavior change.
  * Fix ACL setting in ikisite enable to come after chmod, which otherwise
    would mess up the ACLs and break anonymous git push.
  * Added hostname\_stopwords config that can be used to prevent use of
    particular words in names of sites.
  * ikisite enable, ikisite letsencrypt: When a fullchain file is available,
    use it for apache's SSLCertificateFile, and do not use the chain file.
    This avoids a problem with leteencrypt and apache where apache serves
    up 2 copies of the certificate, one from the cert file, and one from
    the fullchain file, which confuses some SSL clients."""]]