ikiwiki-hosting-web.postinst does `a2enmod userdir` so that it can use
`UserDir disabled`. This seems a little silly.

My `ready/userdir` [[branch|patch]] fixes that.

<http://git.pseudorandom.co.uk/smcv/ikiwiki-hosting.git/commitdiff/refs/heads/ready/userdir>

--[[smcv]]

> What about upgrades?
> 
> Anyway, I think that the idea here was to make the confuration work even
> if used alongside an apache that did have userdir configured for other
> sites. --[[Joey]]

>> Look at the patch, I think I already addressed both objections :-)
>>
>> There are two changes:
>>
>> * don't force mod_userdir on in the postinst; upgrades unaffected.
>> * wrap the `UserDir disabled` directive in `<IfModule mod_userdir.c>`,
>>   so that if it's loaded, we disable it, but if it isn't, we do nothing.
>>   Upgraded machines (and machines where the sysadmin wants userdirs on
>>   another vhost) will load mod_userdir, but disable it for ikisite-managed
>>   vhosts; new installations won't need to load it at all.
>>
>> --[[smcv]]

>>> [[merged|done]] --[[Joey]]
