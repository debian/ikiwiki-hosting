#!/usr/bin/perl
# Ikiwiki setup automator.
# 
# This setup file causes ikiwiki to create a wiki, check it into revision
# control, generate a setup file for the new wiki, and set everything up.
#
# This is modified to run noninteractively; configuration
# is passed in via the environment.
#
# BTW, when you change something in this file, you probably will also want to
# use `ikisite changesetup` to make the same change to existing, running
# sites. The configuration part of the file is written using YAML to ease
# doing so.
use IkiWiki::Setup::HostingAutomator(<<'EOF');
---
force_overwrite: 1
setuptype: Yaml
wikiname: '$ENV{IKIWIKI_WIKINAME}'
adminuser:
 - '$ENV{IKIWIKI_ADMIN}'
adminemail: '$ENV{IKIWIKI_ADMINEMAIL}'
owner: '$ENV{IKIWIKI_OWNER}'
created: '$ENV{IKIWIKI_CREATED}'
hostname: '$ENV{IKIWIKI_HOSTNAME}'
rcs: '$ENV{IKIWIKI_VCS}'
libdir: '$ENV{HOME}/.ikiwiki'
srcdir: '$ENV{IKIWIKI_SRCDIR}'
repository: '$ENV{IKIWIKI_REPOSITORY}'
destdir: '$ENV{IKIWIKI_DESTDIR}'
dumpsetup: '$ENV{HOME}/ikiwiki.setup'
url: 'http://$ENV{IKIWIKI_HOSTNAME}'
cgiurl: 'http://$ENV{IKIWIKI_HOSTNAME}/ikiwiki.cgi'
historyurl: 'http://source.$ENV{IKIWIKI_HOSTNAME}/?p=source.git;a=history;f=[[file]];hb=HEAD'
diffurl: 'http://source.$ENV{IKIWIKI_HOSTNAME}/?p=source.git;a=blobdiff;f=[[file]];h=[[sha1_to]];hp=[[sha1_from]];hb=[[sha1_commit]];hpb=[[sha1_parent]]'
cgi_wrapper: '$ENV{IKIWIKI_CGIDIR}/ikiwiki.cgi'
openid_realm: '$ENV{IKIWIKI_OPENID_REALM}'
openid_cgiurl: 'http://$ENV{IKIWIKI_HOSTNAME}/ikiwiki.cgi'
add_plugins:
 - goodstuff
 - websetup
 - 404
 - ikiwikihosting
 - recentchangesdiff
rss: 1
atom: 1
syslog: 1
hardlink: 1
websetup_force_plugins:
 - httpauth
 - openid
 - emailauth
 - mdwn
 - wmd
 - aggregate
websetup_unsafe:
 - url
 - cgiurl
 - verbose
 - syslog
 - usedirs
 - prefix_directives
 - indexpages
 - repositories
websetup_show_unsafe: 0
cgi_wrappermode: $ENV{IKIWIKI_CGI_WRAPPERMODE}
git_wrappermode: $ENV{IKIWIKI_GIT_WRAPPERMODE}
untrusted_committers:
 - $ENV{IKIWIKI_GITDAEMONUSER}
anonpush: 1
ENV:
 TMPDIR: '$ENV{HOME}/tmp'
timezone: GMT
EOF
